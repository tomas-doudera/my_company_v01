

$(document).ready(function () {
    //---------------------------------------------------------------------show main nav by burger------------------------------------------------------------------------
    //Description: When is burger icon showed (page width <992px) and then clicked.This code put class activeMainNavBur to #mainNav which show us #mainNav.
    $("#burger").click(function () {
        $("#mainNav").toggleClass("activeMainNavBur");
    });

    //---------------------------------------------------------------------------------------------------------------------------------------------------------------------


    //-------------------------------------------------------------------------Carousel---------------------------------------------------------------------------------
    //Description:
    //1.Declaration of var.
    //2.First run of carusel by default value.   
    //3.Function for change of interval of changing the carusel.


    //1.Declaration of var.
    var i = 2;
    var timeUncountVar = 2;
    //set values time for change of carusel
    var delayCarusel = 3000;
    var delayOnClick = 10000;
    //first set of interval for changing of carusel
    var intervalcounting = setInterval(unCountTime, 1000);



    //2.First run of carusel by default value.
    changeInterval(delayCarusel);
    //3.Function for change of interval of changing the carusel.
    function changeInterval(changeDelay) {
        timeUncountVar = changeDelay / 1000;
        clearInterval(intervalcounting);
        intervalcounting = setInterval(unCountTime, 1000);
    }



    //----------------------set interval carusel----------------------------------
    function unCountTime() {
        timeUncountVar -= 1;
        if (timeUncountVar < 1) {
            changeInterval(delayCarusel);
            i++;
            changeCarusel();
        }
    }
    //----------------------------------------------------------------------------



    //------------------------Auto change of carusel------------------------------
    function changeCarusel() {
        if (i > 3) {
            i = 1;
        }
        if (i < 1) {
            i = 3;
        }
        addActive(i);
        /* console.log(delayCarusel); */
    }
    //----------------------------------------------------------------------------


    //----------------------Add class active--------------------------------------
    function addActive(indexOfElement) {
        $(".mySlides").removeClass("activated");
        $(".dot").removeClass("activeDot");
        $("#dot" + indexOfElement).addClass("activeDot");
        $("#slider" + indexOfElement).addClass("activated");
    }

    //----------------------------------------------------------------------------


    //--------------------change carusel by click on specific dot-----------------
    $(".dot").on("click", function () {
        i = Number($(this).attr("id").slice(3, 4));
        changeCarusel();
        changeInterval(delayOnClick);
    });
    //----------------------------------------------------------------------------



    //--------------------change carusel on click on arrow------------------------
    $(".arrow-carusel").on("click", function () {
        //alert("click");
        if ($(this).attr("id") == "prevSlide") {
            //alert(i);
            i--;
            changeCarusel();
            changeInterval(delayOnClick);
        } else if ($(this).attr("id") == "nextSlide") {
            //alert("next");
            i++;
            changeCarusel();
            changeInterval(delayOnClick);
        }
    });

    //----------------------------------------------------------------------------




    //--------------------------------------------------------------------------------------------------------------------------------------------------------------------



    //------------------------------------------------------------------------- photo modal ------------------------------------------------------------------------------	
    //1.Declaratin of var.
    //2.Show clicked photo in modal.
    //3.Close modal on click on close modal span.X

    //1.Declaratin of var.
    var $modal = $('#myModal');
    var $imgAll = $(".imgBox img");
    var $modalImg = $("#myModal img");
    var $modalSpam = $(".close");
    var $lastOpenPhoto;


    //2.Show clicked photo in modal.
    $imgAll.on("click", function () {
        $modal.addClass("modalActive");
        $modalImg.attr("src", $(this).attr("src"));
        $("body").addClass("modalPrevent");
        $lastOpenPhoto = $(this);
    });

    //3.Close modal on click on close modal span.X
    $modalSpam.on("click", function () {
        $(".modal").removeClass("modalActive");
        $("body").removeClass("modalPrevent");
    });

    //4.Close modal on ESC.
    $(document).keyup(function (e) {
        if ((e.keyCode === 27) && ($(".modal").hasClass("modalActive"))) {
            $(".modal").removeClass("modalActive");
            $("body").removeClass("modalPrevent");
        }
    });

    //------------------------------------------------------------------------back button show----------------------------------------------------------------------------
    //Show back button when page is scrolled under 82px.

    $(window).on("scroll", function () {
        if ($(window).scrollTop() > 82) {
            $("#backButton").addClass("active");
        } else {
            $("#backButton").removeClass("active");
        }
    });

    //--------------------------------------------------------------------------------------------------------------------------------------------------------------------


    //-------------------------------------------------------------------------Change by scroll-------------------------------------------------------------------------	
    //1.Declaration of var.
    //2.Declaration of array for nav bar anchors.
    //3.First check where is page scrolled.
    //4.If page is resized. Reinitialize the array.
    //5.Check where is page scrolled on scroll.
    //6.Slide animation when user click on page section anchor.
    //7.First animation objects with class slideanim. They slide on page from nowhere.



    //1.Declaration of var.
    var $arrayOfNavBar = [];
    var $numberOfAnchorInNav = 0;
    var $headerHeight = parseInt($("header").css("height"));
    //2.Declaration of array for nav bar anchors.
    funGetAnchorsFromNavBarToArray();
    //3.first check where is page scrolled.
    checkWhereIsScrolled();

    //4.If page is resized. Reinitialize the array.
    $(window).resize(function () {
        clearTimeout(window.resizedFinished);
        window.resizedFinished = setTimeout(function () {
            funGetAnchorsFromNavBarToArray();
            checkWhereIsScrolled();
        }, 250);
    });

    //Get all archor from mainNav
    function funGetAnchorsFromNavBarToArray() {
        $("#mainNav a").each(function () {
            if (this.hash !== "") {
                $arrayOfNavBar[$numberOfAnchorInNav] = [];
                $arrayOfNavBar[$numberOfAnchorInNav][0] = $(this).attr("href");
                $arrayOfNavBar[$numberOfAnchorInNav][1] = $($(this).attr("href")).offset().top;
                $arrayOfNavBar[$numberOfAnchorInNav][2] = "#mainNav " + "[href='" + $arrayOfNavBar[$numberOfAnchorInNav][0] + "']";
                $numberOfAnchorInNav++;
            }
        });
    }

    //5.Check where is page scrolled on scroll.
    $(window).on("scroll", function () {
        checkWhereIsScrolled();
    });
    function checkWhereIsScrolled() {
        if ($(window).scrollTop() + $headerHeight > $arrayOfNavBar[0][1]) {
            var a = $numberOfAnchorInNav - 1;
            $("#mainNav a").removeClass("activeMainNav");
            while (a > -1) {
                if (checkSectionWhereIsPageScrolled($arrayOfNavBar[a][1])) {
                    $($arrayOfNavBar[a][2]).addClass("activeMainNav");
                    break;
                }
                a--;
            }
        }
    }
    function checkSectionWhereIsPageScrolled(offsetOfSection) {
        if ($(window).scrollTop() + $headerHeight > offsetOfSection) {
            return true;
        }
    }

    //6.Slide animation when user click on page section anchor.
    $("a").on('click', function (event) {
        if (this.hash !== "") {
            event.preventDefault();
            var hash = this.hash;
            // Using jQuery's animate() method to add smooth page scroll
            // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
            $('html, body').animate({
                scrollTop: $(hash).offset().top - $headerHeight + 2
            }, 800);
        } // End if
    });


    //7.First animation objects with class slideanim. They slide on page from nowhere.
    $(window).scroll(function () {
        $(".slideanim").each(function () {
            var pos = $(this).offset().top;
            var winTop = $(window).scrollTop();
            if (pos < winTop + 600) {
                $(this).addClass("slide");
            }
        });
    });

    //--------------------------------------------------------------------------------------------------------------------------------------------------------------------


    //------------------------------------------------------------------------Pricing form--------------------------------------------------------------------------------
    //1.Decleration of variables.	
    //2.Click on any price button. It active modal for order form.
    //3.Form validation before send on server.  
    //4.Creation of warning table for customer that something is wrong fullfilled.   
    //5.Validation of email.

    //1.Declaratin of var.
    var $modalpricetable = $("#modalPricing");
    var $priceTableButton = $(".priceTable button");

    //2.Click on any price button. It active modal for order form. 
    $priceTableButton.on("click", function () {
        var $priceButtonValue = $(this).val();
        $modalpricetable.addClass("modalActive");
        $("body").addClass("modalPrevent");
        $("#choise").val($priceButtonValue);
    });

    //3.Form validation before send on server.
    $("#formValidation").on("click", function () {
        $(".helpTab").remove();
        if ($("#fname").val() === "") {//First name validation if anything is fullfilled.
            event.preventDefault();//Stop of sending request to server
            putHelpTable($("#fname"), "!!!!Please fill first name!!!!");//Creation of message for customer that something is wrong fullfilled.
        }
        if ($("#lname").val() === "") {//Last name validation if something is fullfilled.
            event.preventDefault();//Stop of sending request to server
            putHelpTable($("#lname"), "!!!!Please fill last name!!!!");//Creation of message for customer that something is wrong fullfilled.
        }
        if (($("#mail").val() === "") || (!validateEmail($("#mail").val()))) {//Mail validation if something is fullfilled and if email is corrected wrote.
            event.preventDefault();//Stop of sending request to server
            putHelpTable($("#mail"), "!!!!Please fill correct email as tomas@mail.com!!!!");//Creation of message for customer that something is wrong fullfilled.
        }
    });

    //4.Creation of warning table for customer that something is wrong fullfilled. 
    function putHelpTable(elementWhereToPut, message) {
        messageTable = "<div class='helpTab'>";
        messageTable += "<p>";
        messageTable += message;
        messageTable += "</p>";
        messageTable += "</div>";
        elementWhereToPut.before(messageTable);
    }
    //5.Validation of email.
    function validateEmail($email) {
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        return emailReg.test($email);
    }


    //--------------------------------------------------------------------------------------------------------------------------------------------------------------------



    //------------------------------------------------------------------------- Google maps ------------------------------------------------------------------------------	
 
    myMap();
    function myMap() {
        var myCenter = new google.maps.LatLng(34.0086832, -118.5324026);
        var mapProp = {center: myCenter, zoom: 10, scrollwheel: true, draggable: true, mapTypeId: google.maps.MapTypeId.ROADMAP};
        var map = new google.maps.Map(document.getElementById("googleMap"), mapProp);
        var marker = new google.maps.Marker({position: myCenter});
        marker.setMap(map);
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------------------------------
});











